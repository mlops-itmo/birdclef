import os
import random

import click
import mlflow
import numpy as np
import pandas as pd
import torch
import torchaudio
import yaml
from dotenv import load_dotenv

load_dotenv()
os.environ["AWS_ACCESS_KEY_ID"] = os.getenv("AWS_ACCESS_KEY_ID")
os.environ["AWS_SECRET_ACCESS_KEY"] = os.getenv("AWS_SECRET_ACCESS_KEY")
os.environ["MLFLOW_S3_ENDPOINT_URL"] = os.getenv("MLFLOW_S3_ENDPOINT_URL")

PATH_MODEL_TO_S3 = "s3://mlflow/1/56aa91491a4e474399a3524b2d3804bf/artifacts/model"
DEVICE = torch.device("cpu")


class ResNetInference:
    duration = 8000
    sr = 32000
    channel = 3
    shift_pct = 0.4
    path_to_label = "./src/configs/labels.cfg.yml"

    def __init__(self):
        self.model = mlflow.pytorch.load_model(PATH_MODEL_TO_S3)
        self.model.to(DEVICE)
        self.model.eval()

        with open(self.path_to_label) as file:
            self.labels = yaml.safe_load(file)["labels"]

    @staticmethod
    def open_data(audio_file: str):
        sig, sr = torchaudio.load(audio_file)
        return sig, sr

    @staticmethod
    def rechannel(aud, new_channel):
        sig, sr = aud

        if sig.shape[0] == new_channel:
            # Nothing to do
            return aud

        if new_channel == 1:
            # Convert from stereo to mono by selecting only the first channel
            resig = sig[:1, :]
        else:
            # Convert from mono to stereo by duplicating the first channel
            resig = torch.cat([sig, sig, sig])

        return (resig, sr)

    @staticmethod
    def resample(aud, newsr):
        sig, sr = aud

        if sr == newsr:
            # Nothing to do
            return aud

        num_channels = sig.shape[0]
        # Resample first channel
        resig = torchaudio.transforms.Resample(sr, newsr)(sig[:1, :])
        if num_channels > 1:
            # Resample the second channel and merge both channels
            retwo = torchaudio.transforms.Resample(sr, newsr)(sig[1:, :])
            resig = torch.cat([resig, retwo])

        return (resig, newsr)

    @staticmethod
    def pad_trunc(aud, max_ms):
        sig, sr = aud
        num_rows, sig_len = sig.shape
        max_len = sr // 1000 * max_ms

        if sig_len > max_len:
            # Truncate the signal to the given length
            sig = sig[:, :max_len]

        elif sig_len < max_len:
            # Length of padding to add at the beginning and end of the signal
            pad_begin_len = random.randint(0, max_len - sig_len)
            pad_end_len = max_len - sig_len - pad_begin_len

            # Pad with 0s
            pad_begin = torch.zeros((num_rows, pad_begin_len))
            pad_end = torch.zeros((num_rows, pad_end_len))

            sig = torch.cat((pad_begin, sig, pad_end), 1)

        return (sig, sr)

    @staticmethod
    def time_shift(aud, shift_limit):
        sig, sr = aud
        _, sig_len = sig.shape
        shift_amt = int(random.random() * shift_limit * sig_len)
        return (sig.roll(shift_amt), sr)

    @staticmethod
    def spectro_gram(aud, n_mels=64, n_fft=1024, hop_len=None):
        sig, sr = aud
        top_db = 80

        # spec has shape [channel, n_mels, time],
        # where channel is mono, stereo etc
        spec = torchaudio.transforms.MelSpectrogram(
            sr, n_fft=n_fft, hop_length=hop_len, n_mels=n_mels
        )(sig)

        # Convert to decibels
        spec = torchaudio.transforms.AmplitudeToDB(top_db=top_db)(spec)
        return spec

    @staticmethod
    def spectro_augment(spec, max_mask_pct=0.1, n_freq_masks=1, n_time_masks=1):
        _, n_mels, n_steps = spec.shape
        mask_value = spec.mean()
        aug_spec = spec

        freq_mask_param = max_mask_pct * n_mels
        for _ in range(n_freq_masks):
            aug_spec = torchaudio.transforms.FrequencyMasking(freq_mask_param)(
                aug_spec, mask_value
            )

        time_mask_param = max_mask_pct * n_steps
        for _ in range(n_time_masks):
            aug_spec = torchaudio.transforms.TimeMasking(time_mask_param)(
                aug_spec, mask_value
            )

        return aug_spec

    def preprocessing_data(self, path_audio: str) -> torch.Tensor:
        audio = self.open_data(path_audio)
        reaud = self.resample(audio, self.sr)
        rechan = self.rechannel(reaud, self.channel)
        dur_aud = self.pad_trunc(rechan, self.duration)
        shift_aud = self.time_shift(dur_aud, self.shift_pct)
        sgram = self.spectro_gram(shift_aud, n_mels=64, n_fft=1024, hop_len=None)
        aug_sgram = self.spectro_augment(
            sgram, max_mask_pct=0.1, n_freq_masks=2, n_time_masks=2
        )
        aug_sgram_m, aug_sgram_s = aug_sgram.mean(), aug_sgram.std()
        aug_sgram = (aug_sgram - aug_sgram_m) / aug_sgram_s

        return aug_sgram.unsqueeze(dim=0)

    def postprocessing(self, probabilities: np.ndarray) -> pd.DataFrame:
        df_result = pd.DataFrame(data=probabilities, columns=self.labels)

        return df_result

    def predict_model(
        self,
        path_audio: str,
    ) -> None:
        print("=== Start data prepare ===")
        prepare_audio = self.preprocessing_data(path_audio)
        print("=== End data prepare ===")

        print("=== Start model inference ===")
        model_output = self.model(prepare_audio)
        probabilities = (
            torch.nn.functional.softmax(model_output, dim=0).detach().numpy()
        )
        print("=== End model inference ===")

        print("=== Start postprocessing model output ===")
        df_return = self.postprocessing(probabilities)
        print("=== End postprocessing model output ===")

        print("=== Result ===")
        print(df_return)


model = ResNetInference()


@click.command()
@click.option("--path_audio", type=click.Path(exists=True))
def run_inference(path_audio: str):
    model.predict_model(path_audio)


if __name__ == "__main__":
    run_inference()

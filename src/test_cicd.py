def add_numbers(x, y):
    return x + y


def test_add_numbers():
    assert add_numbers(1, 2) == 3
    assert add_numbers(-1, 1) == 0
    assert add_numbers(10, -5) == 5


if __name__ == "__main__":
    test_add_numbers()
    some_long_string = (
        "Lorem ipsum dolor sit amet," "consectetur adipiscing elit. Nulla vel nunc"
    )

    print("All tests passed.")

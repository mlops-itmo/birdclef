Birdclef
==============================
В данной работе использовались данные и код, представленный в соревновании на платформе Kaggle - [BirdCLEF 2023](https://www.kaggle.com/competitions/birdclef-2023)

Описание поставленной задачи: [техническое задание](docs/specification.md)
# Результат выполнения 
## №1 Шаблонизация. Python пакеты и CLI.
**Результаты**:
- В качестве шаблона мы выбрали "Cookiecutter Data Science", Отдельно стоит отметить, что часть директорий мы убрали, так как они нам не пригодились.

**Артефакты**:
- [MR#2](https://gitlab.com/mlops-itmo/birdclef/-/merge_requests/2)
## №2 Codestyle, инструменты форматирования, линтеры
**Результаты**:
- внутри команды обговорены общие правила для оформления кода
- произведена интеграция линтеров в IDE (VSCode, PyCharm)  
- настроен [CICD пайплайн](.gitlab-ci.yml) для проверки кода на стиль и семантику

**Артефакты**:
- [MR#1](https://gitlab.com/mlops-itmo/birdclef/-/merge_requests/1)
- [MR#2](https://gitlab.com/mlops-itmo/birdclef/-/merge_requests/2)

## №3 Хранение и версионирование кода
**Результаты**:
- Для хранения и версионирования кода мы выбрали следующую стуктуру. Ввиду того, что в данном репозитории мы рассматриваем два решения с разным окружением, было решено каждый эксперимент вести в отдельной ветке. Но при этом для каждой из веток есть "общие" инфраструктурные решения (структура папок, CICD, исходный набор данных), который мы поместили в main.
- Для отработки Github Flow мы работали с веткой main на этапе реализации инфраструктурных решений 

**Артефакты**:
- [.gitignore-фалй](.gitignore)
- [MR#1](https://gitlab.com/mlops-itmo/birdclef/-/merge_requests/1)
- [MR#2](https://gitlab.com/mlops-itmo/birdclef/-/merge_requests/2)
- [MR#3](https://gitlab.com/mlops-itmo/birdclef/-/merge_requests/3)

## №4 Gitlab CI
**Результаты**:
- был развернут отдельный runner на удаленном сервере
- был реализован пайплайн, который выполняет проверку кода с помощью инструментов black, isort, flake8

**Артефакты**:
- [страница с CICD](https://gitlab.com/mlops-itmo/birdclef/-/pipelines)
- Информация о созданном runner'e
  ![](reports/figures/gitlab-runners-2.png)

## №5 Менеджмент зависимостей
**Результаты**:
- В качестве менеджера зависимостей был выбран pip.
- Дополнительно стоит отметить, что ввиду того, что в проекте рассматривается два решения с разной инфраструктурой, мы выделяем два уровня зависимотей. Первый - базовый уровень, в него входят зависимости, которые необходимы для работы инфраструктуры (dvc, boto3, click, mlflow). Второй - надстройка над базовым для работы конкретных решений.

**Артефакты**:
- [[main] requirements.txt](https://gitlab.com/mlops-itmo/birdclef/-/blob/main/requirements.txt) 
- [[pretrain-all-you-need] requirements.txt](https://gitlab.com/mlops-itmo/birdclef/-/blob/pretrain-all-you-need/requirements.txt)
- [[resnet-model] requirements.txt](https://gitlab.com/mlops-itmo/birdclef/-/blob/resnet-model/requirements.txt?ref_type=heads) 

## №6 CLI + запускатор
**Результаты**:
- реализовано CLI для основных модулей
- для создания единой точки входа запуска пайплайна использовался инструмент dvc, который позволяет выстраивать пайплайны

**Артефакты**:
  - resnet-model
    - [dvc.yaml - описание пайплайна](https://gitlab.com/mlops-itmo/birdclef/-/blob/resnet-model/dvc.yaml)
    - [make_dataset.py - подготовка набора данных](https://gitlab.com/mlops-itmo/birdclef/-/blob/resnet-model/src/data/make_dataset.py)
    - [train_model.py - обучение модели](https://gitlab.com/mlops-itmo/birdclef/-/blob/resnet-model/src/models/train_model.py)
  - pretrain-all-you-need
    - [dvc.yaml - описание пайплайна](https://gitlab.com/mlops-itmo/birdclef/-/blob/pretrain-all-you-need/dvc.yaml)
    - [make_dataset.py - подготовка набора данных](https://gitlab.com/mlops-itmo/birdclef/-/blob/pretrain-all-you-need/src/data/make_dataset.py)
    - [build_features.py - выделение обучаемых признаков](https://gitlab.com/mlops-itmo/birdclef/-/blob/pretrain-all-you-need/src/features/build_features.py)
    - [train_model.py - обучение модели](https://gitlab.com/mlops-itmo/birdclef/-/blob/pretrain-all-you-need/src/models/train_model.py)

## №7 DVC + S3
**Результаты**:
- сконфигурирован DVC с хранением данных на удаленном сервере
- в DVC были добавлены исходные данные, предобученные модели, артефакты работы экспериментов
- был настроен пайплайн для обучения моделей с помощью DVC

**Артефакты**:
- [resnet-model - dvc.yaml - описание пайплайна](https://gitlab.com/mlops-itmo/birdclef/-/blob/resnet-model/dvc.yaml)
- [pretrain-all-you-need - dvc.yaml - описание пайплайна](https://gitlab.com/mlops-itmo/birdclef/-/blob/pretrain-all-you-need/dvc.yaml)
- [исходные данные](https://gitlab.com/mlops-itmo/birdclef/-/tree/main/data/raw)
- [используемые модели](https://gitlab.com/mlops-itmo/birdclef/-/tree/pretrain-all-you-need/models)
- [открытая часть конфига](https://gitlab.com/mlops-itmo/birdclef/-/blob/main/.dvc/config?ref_type=heads)

## №8 MLFlow
- реализован распределенный вариант запуска MLFlow
- добавлена аутентификация по логину и пароля для входа в MLFlow
- в исходный код обучения моделей было добавлено логирование параметров, метрик и моделей

**Артефакты**:
- [docker-compose](docker/docker-compose.yaml) с настройкой инфраструктуры (PostgreSQL + PGAdmin + Minio + MLFlow)
- [pretrain-all-you-need - train_model.py - обучение модели](https://gitlab.com/mlops-itmo/birdclef/-/blob/pretrain-all-you-need/src/models/train_model.py)
- [resnet-model - train_model.py - обучение модели](https://gitlab.com/mlops-itmo/birdclef/-/blob/resnet-model/src/models/train_model.py)

![](reports/figures/mlflow.png)


## Организация проекта
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A docs
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
